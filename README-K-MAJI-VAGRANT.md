# k-maji-enterprise

<p align="center">
  <img src="https://gitlab.com/kronops/images/raw/master/images/kamaji.jpg" width=150>
</p>

## Construyendo un Sistema de Orquestación de TI con Ansible y Jenkins

## Contenidos

1. [Introducción](#introducción)
2. [Requisitos](#requisitos)
3. [Preparación](#preparación)
4. [Despliegue](#despliegue)
5. [Referencias](#referecias)

## Introducción

Esta es la guía que te permitirá construir un sistema Orquestación de TI con Ansible y Jenkins. Diseñado para ayudar a optimizar las tareas de rutina y mantenimientos de los equipos de operaciones de TI, tanto en los procesos operativos, como en los de infraestructura.

<center>
<img src="https://gitlab.com/kronops/images/raw/master/arch/k-maji-enterprise-arch.png" width="700" align="center">
</center>

### Objetivos

El objetivo de esta guía es simplificar el proceso de construcción de un sistema de orquestación de TI, los objetivos principales son:

- Construir un ambiente de desarrollo local con vagrant
- Personalizar la construcción del ambiente

## Requisitos

### Ambiente de desarrollo local

Se recomienda que los desarrolladores tengan la capacidad de construir un ambiente de desarrollo local que sea parecido a los ambientes de pruebas y producción, por lo tanto se ha facilitado crear un ambiente basado en vagrant, de manera que desarrollador pueda construir el sistema desde su laptop o equipo de escritorio, se necesita lo siguiente:

 * Ubuntu 16.04 (Xenial) Linux Desktop
 * VirtualBox 5.2
 * Vagrant 2.1
 * git
 * make

 *** NOTA: Es recomendable tener instalados los paquetes actualizados de  [VirtualBox](https://www.virtualbox.org/wiki/Downloads) y [Vagrant](https://www.vagrantup.com/downloads.html) ambos descargados desde sus sitios oficiales, ya que los paquetes que comúnmente proveen los asistentes de instalación del Sistema Operativo, no son las versiones soportadas. Por Ej: Ubuntu instala versiones anteriores de Virtual Box y Vagrant.

## Preparación

Si ya estan instalados virtualbox y vagrant, obtener las siguientes dependencias para construir el ambiente:

```shell
$ sudo apt-get install git make
$ git clone https://gitlab.com/kronops/k-maji-enterprise.git
$ cd k-maji-enterprise

```

### Crear archivo de variables de ambiente .env

Para especificar valores de ambiente de las máquinas virtuales utilizamos el archivo .env.
Cree un archivo nuevo con el nombre .env en la carpeta raíz *k-maji-enterprise* con el siguiente contenido (adaptando el contenido a su ambiente):

```shell
# Descomente y especifíque valores válidos en la variable PROXY_URL si se conecta a internet a través de un proxy
#export PROXY_URL=http://myproxy.com:8080
export PROJECT_NAME=local
export PROJECT_ENV=development
export PROJECT_DOMAIN=example.com
export SSH_TMP_USER=vagrant
export DEPLOYER=192.168.33.10
export TESTER=192.168.33.5
export CMDB=192.168.33.7
export MONITOR=disabled
export POSTGRES=disabled
export CMS=disabled
export DNS_PUBLIC=example.com
export DNS_PRIVATE=example.com
export CMDB_SAMPLE_DATA=yes
export BRANCH=master
```

#### Configurar Proxy (Opcional)

Si usted se conecta a internet a través de un proxy primero debe modificar el archivo **ansible\group_vars\all** y especificar los valores de proxy_host, proxy_port, proxy_login y proxy_password de acuerdo al proxy que vaya a utilizar.

```bash
#Configuración de Proxy
#Descomentar las variables de proxy de acuerdo a sus necesidades
#proxy_host: myproxy.com
#proxy_port: 8080
#proxy_login:
#proxy_password:
proxy_url: "http://{{proxy_login}}:{{proxy_password}}@{{proxy_host}}:{{proxy_port}}"
no_proxy: "localhost"
jenkins_no_proxy: "localhost\n127.0.0.1"
proxy_env:
  #fake_param: fake_value
  #http_proxy: "{{proxy_url}}"
  #https_proxy: "{{proxy_url}}"
  #ftp_proxy: "{{proxy_url}}"
  no_proxy: "{{no_proxy}}"
  #HTTP_PROXY: "{{proxy_url}}"
  #HTTPS_PROXY: "{{proxy_url}}"
  #FTP_PROXY: "{{proxy_url}}"
  NO_PROXY: "{{no_proxy}}"

```


Cree el ambiente de desarrollo local usando Vagrant:

```shell
user@host:k-maji-enterprise$ vagrant up
```

Conéctese al servidor deployer:

```shell
user@host:~/k-maji-enterprise$ vagrant ssh deployer
vagrant@deployer:~$ sudo su
root@deployer:~# cd /vagrant
```

## Despliegue

Ejecute el script parametrizando el proyecto "local" y el ambiente "development".


```shell
root@deployer:~/vagrant# bash bin/deploy-enterprise-vagrant.sh
```
El proceso de despliegue pedira el password del usuario y el password del root, ambos son "vagrant".

Terminado el despliegue, es posible acceder al portal del servidor Jenkins con un navegador, en la maquina local por el puerto 1088
http://localhost:1088.

Para iniciar sesion en el portal de Jenkins:
- Usuario: admin
- Password: admin

![Jenkins](https://gitlab.com/kronops/images/raw/master/arch/k-maji.png)


## Referencias

En esta sección hacemos referencia a documentación que usamos de ejemplo para construir este sistema y su integración con otras herramientas de Orquestación de TI.

* GitLab - https://gitlab.com
* GitLab Flow - https://about.gitlab.com/2014/09/29/gitlab-flow/
* The 11 Rules of GitLab Flow - https://about.gitlab.com/2016/07/27/the-11-rules-of-gitlab-flow/
* THE INSIDE PLAYBOOK - ORCHESTRATION, YOU KEEP USING THAT WORD http://www.ansible.com/blog/orchestration-you-keep-using-that-word
* Jenkins - Building a software project - https://wiki.jenkins-ci.org/display/JENKINS/Building+a+software+project
* ImmutableServer - http://martinfowler.com/bliki/ImmutableServer.html
* What do “immutable servers” and AWS environments have in common? - https://elasticbox.com/blog/immutable-server-environments-in-aws/
* Rethinking building on the cloud: Part 4: Immutable Servers - https://www.thoughtworks.com/insights/blog/rethinking-building-cloud-part-4-immutable-servers
* Jenkins - Ansible Plugin - https://wiki.jenkins-ci.org/display/JENKINS/Ansible+Plugin
* Automated Servers and Deployments with Ansible & Jenkins - https://chromatichq.com/blog/automated-servers-and-deployments-ansible-jenkins
* Continuous Integration, Delivery or Deployment with Jenkins, Docker and Ansible - https://technologyconversations.com/2015/02/11/continuous-integration-delivery-or-deployment-with-jenkins-docker-and-ansible/
* Vert.x featuring Continuous Delivery with Jenkins and Ansible - http://vertx.io/blog/vert-x-featuring-continuous-delivery-with-jenkins-and-ansible/