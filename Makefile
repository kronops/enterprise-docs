#
# Makefile for k-maji Enterprise docs on docker
#
# author: Jorge Armando Medina
# desc: Script to build, test and release the k-maji Enterprise docs docker image.

#include /var/lib/jenkins/.env

# DOCKER IMAGE ENV VARS
DOCS_APP_NAME = enterprise-docs
DOCS_APP_VERSION = 1.0.0
DOCS_DOCKER_REPO = ${DOCS_APP_NAME}:${DOCS_APP_VERSION}
APP_PORT = 4000:4000

.PHONY: all build test release clean help

all: help

build:
	@echo "Building ${DOCS_DOCKER_REPO} image."
	docker build -t ${DOCS_DOCKER_REPO} .
	@echo "Listing ${DOCS_DOCKER_REPO} image."
	docker images

test:
	@echo "Run ${DOCS_DOCKER_REPO} image."
	docker run --name ${DOCS_APP_NAME} -p ${APP_PORT} -d ${DOCS_DOCKER_REPO} &
	@echo "Wait until ${DOCS_DOCKER_REPO} is fully started."
	sleep 10
	docker logs ${DOCS_APP_NAME}

release:
	@echo "Push ${DOCS_DOCKER_REPO} image to docker registry."
	cat ${DOCKER_PWD} | docker login --username ${DOCKER_USER} --password-stdin
	docker tag ${DOCS_DOCKER_REPO} ${DOCS_DOCKER_REPO}
	docker push ${DOCS_DOCKER_REPO}

clean:
	@echo ""
	@echo "Cleaning local build environment."
	@echo ""
	docker stop ${DOCS_APP_NAME} 2>/dev/null; true
	docker rm ${DOCS_APP_NAME} 2>/dev/null; true
	@echo ""
	@echo "Purging local images."
	docker rmi ${DOCS_DOCKER_REPO} 2>/dev/null; true

help:
	@echo ""
	@echo "Please use \`make <target>' where <target> is one of"
	@echo ""
	@echo "  build		Builds the docker image."
	@echo "  test		Tests image."
	@echo "  release	Releases images."
	@echo "  clean		Cleans local images."
	@echo ""
	@echo ""
