---
layout: page
title: Arquitectura
nav_order: 4
permalink: /docs/arquitectura
---
# Arquitectura
En el siguiente diagrama se muestra la arquitectura de la solución de
orquestación de TI. Se muestra tanto la capa lógica con los componentes de
software como la capa física con los componentes a nivel servidores y/o
dispositivos de red.

![Arquitectura](/enterprise-docs/assets/images/arquitectura-k-maji.png)

En la capa lógica de componentes se destacan los siguientes grupos:
- **Capa Sources**
  - **SCM Master**: Es el servicio GIT en donde el equipo de operaciones de TI
  almacena el código de la infraestructura.

- **Capa Orquesta**
  - **Jenkins Operations Engine**: Es el servicio de automatización de TI
  basado en Jenkins y sus plugins de interconexión con otros servicios.
  - **Ansible Operations Engine**: Es el sistema de orquestación de TI con el
  cual se automatizan los procesos de aprovisionamiento de infraestructura y
  despliegue de configuraciones y parches en los diferentes servidores.
  - **Testinfra QA Engine**: Es el motor de automatización de pruebas de
  infraestructura, se ejecuta a través de Jenkins y se interconecta a los nodos
  a través de Ansible.
  - **Inspect QA Engine**: Es el motor de automatización de pruebas de
  seguridad.
  - **Selenium QA Engine**: Es el motor de automatización de pruebas
  funcionales Web, usa el motor chromium para probar las aplicaciones web
  desplegadas.
  - **iTop Operations Engine**: Es el servicio en donde está almacenada la
  información del inventario de TI.
  - **Nagios Operations Engine**: Se encarga del monitoreo de la
  infraestructura para prevenir fallas en los servicios.

- **Capa Server Environments**
  - **Infrastructure/Application/Database Server**: Este es el artefacto que
  representa una imagen de sistema operativo para máquina virtual KVM en
  formato QCO2, esta imagen ya tiene integradas las configuraciones de
  hardening y actualizaciones de parches de seguridad aplicados.
