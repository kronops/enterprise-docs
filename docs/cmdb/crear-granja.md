---
layout: page
title: Crear Granja
parent: Administracion de CMDB
nav_order: 5
---

# Crear una granja
En la categoría Virtualización, justo debajo de Infraestructura, dando clic a
*Crear granja* comenzará el registro, se mostrará un formulario como en el
siguiente ejemplo:

![Seccion Virtualización](/enterprise-docs/assets/images/seccion-virtualizacion.png)

## Creación de Granja

![Creación de una Granja](/enterprise-docs/assets/images/creacion-de-granja.png)

Indicar el nombre de la granja y dar clic en crear.

**IMPORTANTE:** Se debe hacer notar que todos los nombres de objetos de
granjas deben escribirse en mayúsculas, una sola palabra, no se deben usar
espacios en blanco, sin embargo si puede usar guión medio ó “-” para usarlo
como separador del nombre.
