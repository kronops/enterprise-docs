---
layout: page
title: Crear un Servidor
parent: Administracion de CMDB
nav_order: 4
---
# Crear un Servidor (Fisico)

En la primera categoría **Infraestructura** se encuentra **Crear Servidor**, al
dar clic iniciara el registro de este:

![Seccion Infraestructura](/enterprise-docs/assets/images/seccion-infraestructura.png)

Al dar clic será dirigido al formulario de creación de objeto. Este tipo de
formulario está presente en la creación de cada objeto de iTop.

![Crear Servidor](/enterprise-docs/assets/images/crear-servidor.png)

El encabezado le mostrara que tipo de objeto está creando, dos barras (superior
e inferior) con los botones **Cancelar** y **Crear**, una barra de pestañas con
las distintas categorías de atributos del objeto y el área de campos para
definir los atributos. Sin importar en cuál pestaña esté localizado, puede
crear el objeto o cancelar el proceso con los botones de las barras superior o
inferior indistintamente.
