---
layout: page
title: Preparacion
parent: Administracion de CMDB
nav_order: 3
---
# Preparación
Antes de empezar a trabajar en la creación de objetos, debe recordar
seleccionar la organización a la cual van a pertenecer los nuevos objetos. Se
recomienda seleccionar siempre su organización.

![Organizacion seleccionada](/enterprise-docs/assets/images/organizacion-seleccionada.png)

Si usted no selecciona una organización, aparecerá así:

![Seleccion de Organización](/enterprise-docs/assets/images/seleccion-de-organizacion.png)


Esto causaría que al crear cada objeto deba definir la organización en sus
atributos, mientras que al estar activa en forma general este atributo se
configurará de forma automática. Otra ventaja de seleccionar la organización es
al momento de realizar  búsquedas, pues estas se enfocarán de forma
predeterminada a objetos de dicha organización.

En el inventario de iTop, cada máquina virtual está supeditada a  un
hypervisor, y este a su vez a una granja, la cual vive en un servidor. Por
consiguiente, para hacer un correcto registro de la infraestructura del
proyecto, se debe de realizar el registro del servidor, la granja y el
hypervisor donde están hospedadas las máquinas virtuales.

Para crear el registro de un ambiente de máquinas virtuales en el inventario de
iTop, en el menú lateral izquierdo dar clic a *Administración de la
Configuración*, se desplegará un submenú. Seleccionar la opción *Resumen de la
infraestructura.*

![Administracion de la Configuración](/enterprise-docs/assets/images/administracion-de-la-configuracion.png)

Esta acción lo llevará a una vista donde se encuentran, agrupados por
categorías, los objetos de la infraestructura que puede llevar registro el
CMDB. En la presente guía nos centraremos en la virtualización, pero siéntase
libre de utilizar todo el registro que considere pertinente al proyecto. Para
más información consulte las siguientes guías:
[Módulo de Manejo de virtualización](https://www.itophub.io/wiki/page?id=2_5_0%3Adatamodel%3Aitop-virtualization-mgmt)
y [Manejo de Servidores](https://www.itophub.io/wiki/page?id=2_5_0%3Adatamodel%3Aitop-config-mgmt#server).
