---
layout: page
title: Crear Hypervisor
parent: Administracion de CMDB
nav_order: 6
---

# Crear un Hypervisor
El proceso para crear un hypervisor es análogo al indicado para crear la granja. Vuelva a la sección de virtualización en la página Resumen de la Infraestructura.

![Creación de un Hypervisor](/enterprise-docs/assets/images/virtualizacion.png)

Un hypervisor está supeditado a una granja. En el menú de selección para el
campo **Granja** encontrará la que ha creado previamente, con lo cual es
posible establecer la relación entre los objetos. Después de definir el nombre
de su hypervisor y haber seleccionado la granja a la que pertenece de clic en
crear.

![Creación de un Hypervisor](/enterprise-docs/assets/images/creacion-de-hypervisor.png)

**IMPORTANTE:** Se debe hacer notar que todos los nombres de objetos de
hipervisores deben escribirse en mayúsculas, una sola palabra, no se deben usar
espacios en blanco, sin embargo si puede usar guión medio ó “-” para usarlo
como separador del nombre.
