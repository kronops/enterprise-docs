---
layout: page
title: Dar de baja Maquina Virtual
parent: Administracion de CMDB
nav_order: 9
---

# Dar de Baja una Máquina virtual

El procedimiento de baja de máquinas virtuales solo es necesario en caso de que
la dirección MAC y la IP desaparezcan del servidor DHCP. No se recomienda
borrar las máquinas como primera opción, pues perderá los tickets e historial
relacionado a ella. Es preferible modificarla a un Estatus **No productivo**, o
en caso de que sea un servicio que se retire, ponerlo como **Obsoleto**.

![Dar de baja Maquina Virtual](/enterprise-docs/assets/images/cambiar-status.png)

Para cambiar el estatus de una máquina virtual, es necesario seguir el
procedimiento de Modificar Máquina Virtual, detallado en el apartado anterior.
Cuando se encuentre en el formulario de la máquina, despliegue el menú frente a
**Estatus** y redefina el valor.

Ahora bien, si va a relacionar la IP a otra MAC y no desea modificar un
registro ya existente, borrar la máquina también eliminará la entrada de
interfaz de red relacionada a ella y toda entrada que se vincule a ella en
cascada. Si está seguro de eliminar el registro de la máquina, deberá
seleccionarla en la lista de Máquinas Virtuales y una vez en sus detalles,
elegir **Otras Acciones** y después **Borrar**.

![Dar de baja Maquina Virtual](/enterprise-docs/assets/images/borrar-maquina-virtual.png)

Esto lo dirigirá a una vista de confirmación, donde le mostrará el nombre del
elemento y todas las entradas vinculadas a este, si está totalmente seguro de
los registros que se perderán pueden eliminarse, pulse el botón **Borrar**.

![Dar de baja Maquina Virtual](/enterprise-docs/assets/images/borrado-de-deployer.png)
