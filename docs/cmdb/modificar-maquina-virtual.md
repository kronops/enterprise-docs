---
layout: page
title: Modificar Maquina Virtual
parent: Administracion de CMDB
nav_order: 8
---

# Modificar Máquina virtual
En caso de que una máquina virtual ya registrada presente cambios o se detecten
errores en sus atributos, es posible modificarla siguiendo estos pasos.
Primero, vaya al listado de máquinas virtuales dando clic a Máquina Virtual en
la página de Resumen de Infraestructura.

![Modificar Maquina Virtual](/enterprise-docs/assets/images/modificar-maquina-virtual.png)

Esto lo direccionará al listado de máquinas virtuales, en este ejemplo la
máquina tester presenta una criticidad diferente a su situación actual.
Seleccionar dando clic en su nombre.

![Modificar Maquina Virtual](/enterprise-docs/assets/images/listado-maquinas-virtuales.png)

Ahora se encuentra en la página de detalles del objeto. En la parte superior
derecha se muestra una serie de acciones que puede ejecuta. Hacer clic en
**Modificar**

![Modificar Maquina Virtual](/enterprise-docs/assets/images/modificar-maquina-virtual-deployer.png)

Al modificar una máquina se mostrará la vista de formulario. Es posible cambiar
cualquier atributo de toda categoría del objeto, incluso definir atributos que
no se habían considerado previamente.

![Modificar Maquina Virtual](/enterprise-docs/assets/images/modificacion-maquina-virtual-deployer.png)

En este caso, se redefinirá la Criticidad para el Negocio a nivel Medio. El
botón Aplicar guardará los cambios. Al finalizar automáticamente regresará a la
página de detalles del objeto con el mensaje de confirmación de los cambios.

![Modificar Maquina Virtual](/enterprise-docs/assets/images/maquina-virtual-deployer-actualizado.png)
