---
layout: page
title: Requisitos para la operación de CMDB
parent: Administracion de CMDB
nav_order: 2
---

# Requisitos para la operación de CMDB

- Instancia de CMDB operando.
- Acceso a la consola de administración de iTop.
- Cuenta de usuario con privilegios de administración de iTop.
- Información previa de la granja de virtualización a la que pertenece el
hipervisor.
- Información previa de los hipervisores donde corren las máquinas virtuales.
- Información previa de máquinas virtuales que se van a cargar en el inventario.


