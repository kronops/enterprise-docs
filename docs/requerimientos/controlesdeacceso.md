---
layout: page
title: Controles de Acceso
parent: Requerimientos
nav_order: 5
---

# Controles de Acceso

Se debe tener documentadas las políticas de control de acceso a los diferentes
componentes del sistema de orquestación de TI. Solo usuarios autorizados, sólo
direcciones IP autorizadas, solo certificados autorizados.

Los servidores de K-MAJI Enterprise y los nodos que administra deben tener
configurado el servicio SSH para permitir conexiones con la cuenta root usando
llave RSA de 4096-bit, por lo tanto debe definir el parámetro
**PermitRootLogin** a **yes** y en caso de que el servicio SSH use el parámetro
**AllowUsers** para limitar el acceso a usuarios autorizados, también se deberá
listar el usuario root, por ejemplo, en el archivo `/etc/ssh/sshd_config`,
deberá tener algo similar a las siguientes líneas:


```
PermitRootLogin yes
AllowUsers sysadmin root
```

Es necesario crear cuentas de usuario autorizadas para las comunicaciones entre
los sistemas. Todos deben contar con contraseñas fuertes y, en caso requerido,
generar  tokens para brindar seguridad a las sesiones HTTPS.

Las llaves SSH deberán generarse en formato RSA de 4096 bits. Estas
proporcionarán  a Ansible los derechos de administración de los nodos.
