---
layout: default
title: Requerimientos
permalink: /docs/requerimientos/
has_children: true
nav_order: 5
---
# Requerimientos

A continuación se puntualizan los requisitos para implementar y operar el
sistema K-MAJI Enterprise en un ambiente de pruebas. Se incluyen los requisitos a
nivel lógico y físico.
