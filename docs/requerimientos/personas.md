---
layout: page
title: Personas
parent: Requerimientos
nav_order: 6
---

# Personas

Para la fase de modelado de procesos es requisito entrevistar al personal
encargado de la ejecucion, asi como a los administradores de los procesos
operativos y de  infraestructura. Se debe de asignar tiempo presencial de los
diferentes stakeholders para que en sesión con personal de Kronops se hable de
cómo ejecutar los procesos. Una vez  realizadas las reuniones, es posible
comenzar a desarrollar los diagramas del modelo de cadena de valor.

Lista usuarios

| Perfil                        | Procesos               | Comentarios |
| ----------------------------- | ---------------------- | ----------- |
| Administrador de Sistemas Sr. | Bob Marley             |             |
| Ingeniero DevOps              | Roger Waters           |             |
| Gerente Operaciones           | Raja Ram               |             |
