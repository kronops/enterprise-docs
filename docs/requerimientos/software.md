---
layout: page
title: Software
parent: Requerimientos
nav_order: 3
---

# Software

La plataforma K-MAJI Enterprise está basada en el sistema operativo GNU/Linux,
estos son los requisitos de sistema operativo para cada servidor.

- **DEPLOYER:** Centos 7 x64.
- **TESTER:** Centos 7 x64.
- **CMDB:** Centos 7 x64.
- **MONITOR:** Centos 7 x64.

**IMPORTANTE:** La instalación de sistema operativo debe de hacerse en la
modalidad **Minimal**, esto para no instalar programas innecesarios para la
plataforma y evitar tener que mantener y asegurar software extra. Todo el
software necesario es automáticamente instalado por los playbooks de K-MAJI
Enterprise y descargado de los repositorios oficiales de la distribución.

El software que se usará en los servidores Deployer, Tester, CMDB y Monitor es
libre, por lo tanto no requiere de ningún tipo de licencia o suscripción para
su uso, basta tener acceso a Internet para descargar los paquetes  de terceros
que requiere K-MAJI Enterprise.
