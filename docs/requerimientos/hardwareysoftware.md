---
layout: page
title: Hardware
parent: Requerimientos
nav_order: 2
---

# Hardware

Estas son las Máquina Virtuales necesarias para implementar el sistema K-MAJI
Enterprise en su infraestructura.

| **Cantidad** | **Servicio** | **vCPU** | **RAM (MB)** | **HDD OS (GB)** | **MAC Address**   | **IP Address** |
| ------------ | ------------ | -------- | ------------ | ------------------- | ----------------- | -------------- |
| 1            | deployer     | 2        | 2048         | 20                  | ab:dd:ef:12:23:45 | 192.168.1.10   |
| 1            | tester       | 2        | 2048         | 20                  | ab:dd:ef:12:23:45 | 192.168.1.11   |
| 1            | cmdb         | 2        | 2048         | 20                  | ab:dd:ef:12:23:45 | 192.168.1.12   |
| 1            | monitor      | 2        | 2048         | 20                  | ab:dd:ef:12:23:45 | 192.168.1.13   |
