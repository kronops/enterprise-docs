---
layout: page
title: Iniciar una ejecución en la vista de tareas
parent: Ejecución de Pipelines
grand_parent: Portal de Operaciones
nav_order: 2
---

# Iniciar una ejecución en la vista de tareas

Presionar el botón **play** que se haya al final de la fila en la tabla de
tareas.

![Desde Vista de Tareas](/enterprise-docs/assets/images/desde-vista-tareas.png)

La tarea comenzará a ejecutarse inmediatamente. Para confirmar que se encuentra
en ejecución, revisar la zona inferior izquierda del portal, donde podrá ver el
**Estado del ejecutor de construcciones**.

![Ejecutor de construcciones](/enterprise-docs/assets/images/estado-ejecutor.png)

Ocasionalmente, a pesar de que la tarea haya iniciado, el indicador de estado
no se actualiza. Antes de intentar presionar de nuevo el botón play, intente
recargar la página para confirmar el estado real de la ejecución.
