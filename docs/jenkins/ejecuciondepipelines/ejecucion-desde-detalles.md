---
layout: page
title: Iniciar una ejecución desde los detalles del pipeline
parent: Ejecución de Pipelines
grand_parent: Portal de Operaciones
nav_order: 3
---

# Iniciar una ejecución desde los detalles del pipeline

Seleccionando el nombre de algún elemento de la tabla de tareas será dirigido a
la **página de detalles** de esa tarea.

![Seleccionar tarea](/enterprise-docs/assets/images/desde-detalles.png)

En el menú lateral izquierdo de la página de detalles se encuentra el botón
**Construir ahora**. Al haberse programado la ejecución del pipeline, podrá
encontrar debajo de ese menú el estado de la ejecución en **Historia de
tareas**.

![Pagina detalles](/enterprise-docs/assets/images/pagina-detalles.png)
