---
layout: page
title: Portal Nagios
parent: Servicio de Monitoreo de Red y Servidores
nav_order: 4
---

Para acceder al *Portal Nagios Core* debera usar un *Browser* y entrar a la
url https://IPMONITOR/nagios

En cuanto se conecte a esa direccion, le sera requerido un *Login* por una
ventana emergente. El Nombre de Usuario por defecto es *nagiosadmin* y el
password *nagiosadmin*. Es posible modificar este acceso por uno mas seguro.

![Login Nagios](/enterprise-docs/assets/images/auth-nagios.png)
