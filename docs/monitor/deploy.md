---
layout: page
title: Despliegue del servicio de Monitoreo
parent: Servicio de Monitoreo de Red y Servidores
nav_order: 3
---

Para iniciar el despliegue automático del servicio de monitoreo, deberá ingresar
al *Portal de Operaciones*, iniciar sesión y seleccionar la vista IT-Deploys

![IT-Deploy Nagios](/enterprise-docs/assets/images/deploy-monitor.png)

Puede iniciar el Pipeline usando cualquiera de los métodos descritos en
[*Ejecución de Pipelines*](/enterprise-docs/docs/jenkins/ejecuciones/)
de la sección [*Portal de Operaciones*] de esta guía.

![Pipeline Nagios](/enterprise-docs/assets/images/pipeline-monitor.png)

Como puede ver en la imagen, el despliegue se realiza en varias etapas:

- **Deploy System:** En esta etapa se prepara el servidor con las
  Configuraciones globales necesarias para el correcto funcionamiento del
  servidor en el pool.
- **Deploy Nagios:** Hace despliegue del Software de monitoreo, lo cual incluye
  todos los requerimientos, servidor web, configurar permisos, plugins, y demás
  tareas post instalación.
- **Test Infra:** En la última etapa se hacen pruebas para asegurar que el
  servicio esté activo y que el servidor web responda con el portal de Nagios Core.

Al terminar el pipeline, tendrá disponible el servicio de monitoreo para los hosts que se encuentren en el inventario de K-MAJI, que por default incluye el
pool inicial (Es decir, la máquina deployer, tester, cmdb y el propio monitor).
