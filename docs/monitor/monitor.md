---
layout: default
title: Servicio de Monitoreo de Red y Servidores
permalink: /docs/monitor/
has_children: true
nav_order: 10
---

# Introducción

En esta seccion se describen los pasos para implementar el servicio de
monitoreo de red Nagios Core en su infraestructura.

## Objetivos

Conocer el metodo de despliegue de Nagios Core desde K-MAJI y familiarizarse con su
uso.
