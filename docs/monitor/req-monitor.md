---
layout: page
title: Requisitos
parent: Servicio de Monitoreo de Red y Servidores
nav_order: 2
---

# Requisitos

- Instancia de K-MAJI operando.
- La direccion IP del servidor de Monitoreo de red y servidores.
- Navegador web.
- Acceso al Portal de Operaciones.

