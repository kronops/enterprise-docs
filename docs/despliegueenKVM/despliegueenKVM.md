---
layout: default
title: Despliegue en Máquinas Virtuales KVM
permalink: /docs/despliegueenKVM/
has_children: true
nav_order: 6
---

# Despliegue en máquinas Virtuales KVM

## Introducción

En esta sección se indican los pasos para desplegar K-MAJI Enterprise en un
ambiente de pruebas basado en máquinas virtuales basado en KVM.

## Objetivos

Se deben seguir estos procedimientos para desplegar la plataforma:

* Describir los requisitos de los servidores virtuales.
* Preparar requisitos de proxy para conexión a Internet.
* Ejecutar el bootstrap de dependencias en los servidores virtuales.
* Definir las variables con los parámetros de despliegue inicial.
* Ejecutar el script deply-k-maji.sh.
