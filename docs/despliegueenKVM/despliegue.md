---
layout: page
title: Despliegue
parent: Despliegue en Máquinas Virtuales KVM
nav_order: 6
---

# Despliegue

Ejecutar el script con privilegios de super user, proporcionar como argumentos
el nombre del proyecto y el ambiente inicial de esta instalación.

```
kronops@deployer:~$ sudo bash deploy-k-maji.sh kronops testing
```

Donde **nombre de proyecto** hace referencia a la organización (empresa,
departamento u equipo de trabajo) a la cual pertenece la nueva instancia de
K-MAJI Enterprise, mientras que **ambiente** es el contexto del pool dentro de
dicha organización.

El orquestador es capaz de manejar la automatización de tareas en múltiples
ambientes (e.g. desarrollo, pruebas, producción) manteniéndolos aislados  unos
de otros a nivel de ejecución e inventario.

El proceso de despliegue pedirá el password de usuario y sudo. El script
instalador utilizará estos logins para establecer los enlaces internos de
K-MAJI Enterprise.

Terminado el despliegue, deberá cambiar contraseñas de usuario y sudo en las
máquinas del orquestador, lo cual no afectará el funcionamiento del sistema.
