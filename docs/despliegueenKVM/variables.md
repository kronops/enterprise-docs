---
layout: page
title: Variables de Instalación
parent: Despliegue en Máquinas Virtuales KVM
nav_order: 5
---

# Variables de Instalación

Deberá proceder a descargar  en la máquina **deployer** el script de
instalación:

```
kronops@deployer:~$ wget https://gitlab.com/kronops/k-maji-enterprise/raw/master/bin/deploy-k-maji.sh
```

Ahora es necesario editar el archivo descargado deploy-k-maji.sh con las
configuraciones de nuestro ambiente:

```
kronops@deployer:~$ vim deploy-k-maji.sh
```
```
## Project constants ##
# Set these vars according to your config, set disabled for not using
ENV_PREFIX="ke-"
SSH_TMP_USER=kronops
DEPLOYER=192.168.1.10
TESTER=192.168.1.11
CMDB=192.168.1.12
MONITOR=192.168.1.13
NFS=disabled
DNS_PUBLIC=kronops.com.mx
DNS_PRIVATE=kronops.com.mx
CMDB_SAMPLE_DATA=no
BRANCH=master
proxy_host="192.168.0.1"
proxy_port="3128"
proxy_login=""
proxy_pass=""
# End constants #
```

**IMPORTANTE:** Las IPs usadas son para fines demostrativos

Al principio del script se definen los datos que utilizará el orquestador como
configuración inicial. Los valores asignados desplegaran un ambiente **testing**
en el pool de servidores.

* **Prefijo:** Se usara para nombrar a las máquinas orquestadoras (e.g. si el
valor fuera “ke-” la máquina 192.168.1.15 se llamaria ke-tester)
* **Usuario:**  Todas las máquinas del pool tienen este usuario.
* **Direcciones IP:** Usamos las direcciones de las máquinas destinadas a cada
servicio, o definimos disabled para el caso que no se vaya a implementar.
* **Dominios:** Los componentes que manejen un servidor web tomarán este valor
para construir sus URL (e.g. itop.kronops.com.mx)
* **Datos dummies para iTop** Si el valor es no, la instancia de iTop (CMDB) se
generará con los inventarios vacíos, en caso contrario, añadirá valores de
muestra.
* **Rama del SCM** La versión estable de K-MAJI Enterprise se encuentra en la
rama production.
* **Configuracion del proxy** Si su ambiente trabaja bajo un servidor proxy
especifique sus direcciones y accesos aquí.
