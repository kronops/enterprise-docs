---
layout: page
title: Preparación
parent: Despliegue en Máquinas Virtuales KVM
nav_order: 3
---

#  Preparación

## Uso de proxy
En caso de que la red trabaje bajo un proxy, las máquinas deberán configurarse
para acceder al mismo. Crear un archivo ```~/.bash_proxy``` para configurar la
salida a proxy durante la instalación:

```
sysadmin@deployer:~$ vim .bash_proxy
```
```
# Proxy vars
http_proxy=http://192.168.0.1:3128
https_proxy=http://192.168.0.1:3128
no_proxy=localhost
HTTP_PROXY=$http_proxy
HTTPS_PROXY=$http_proxy
NO_PROXY=$no_proxy
export http_proxy
export https_proxy
export no_proxy
export HTTP_PROXY
export HTTPS_PROXY
export NO_PROXY
```

**IMPORTANTE:** Los valores IP utilizados en el ejemplo son con fines
demostrativos.

Es indispensable proporcionar tanto la dirección como el puerto. Ahora debe
indicar al shell que utilice las variables añadidas:

```
sysadmin@deployer:~$ source .bash_proxy
```

Para poder ejecutar la instalación correctamente, se requiere la generación del
archivo ```/etc/sudoers.d/sysadmin``` con el siguiente contenido:

```
sysadmin@deployer $ sudo vim /etc/sudoers.d/sysadmin
```
```
%sysadmin ALL=(ALL) NOPASSWD: ALL
```

Varios procesos del despliegue se harán como super user, asi que tambien debe
proporcionar la información del proxy a los sudoers. Para tal efecto, debe
agregar las siguientes líneas a la sección default de las variables que usan
los sudoers:

```
sysadmin@deployer:~$ sudo visudo
```
```
Defaults    env_keep += "HTTP_PROXY HTTPS_PROXY NO_PROXY"
Defaults    env_keep += "http_proxy https_proxy no_proxy"
```
