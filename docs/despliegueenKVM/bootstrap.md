---
layout: page
title: Bootstrap
parent: Despliegue en Máquinas Virtuales KVM
nav_order: 4
---

# Bootstrap

Para comenzar con la instalación de K-MAJI Enterprise, cada nodo de la  
infraestructura (Deployer, Tester, CMDB y Monitor) deberá ejecutar con permisos
de sudo el script que instala las dependencias necesarias.

```
sysadmin@host:~$ wget https://gitlab.com/kronops/k-maji-enterprise/raw/master/bin/bootstrap-node-rhel7.sh
sysadmin@host:~$ sudo bash bootstrap-node-rhel7.sh
```

Una vez que las dependencias estén cubiertas en cada máquina (nodo) del pool de
servidores y el uso de proxy esté correctamente configurado,  deberá preparar el
script de instalación.
